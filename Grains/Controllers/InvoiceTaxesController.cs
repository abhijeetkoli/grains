﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Grains.Models;

namespace Grains.Controllers
{
    public class InvoiceTaxesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/InvoiceTaxes
        public IQueryable<InvoiceTax> GetOrderTaxes()
        {
            return db.InvoiceTaxes;
        }

        // GET: api/InvoiceTaxes/5
        [ResponseType(typeof(InvoiceTax))]
        public async Task<IHttpActionResult> GetInvoiceTax(int id)
        {
            InvoiceTax invoiceTax = await db.InvoiceTaxes.FindAsync(id);
            if (invoiceTax == null)
            {
                return NotFound();
            }

            return Ok(invoiceTax);
        }

        // PUT: api/InvoiceTaxes/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutInvoiceTax(int id, InvoiceTax invoiceTax)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != invoiceTax.Id)
            {
                return BadRequest();
            }

            db.Entry(invoiceTax).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InvoiceTaxExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/InvoiceTaxes
        [ResponseType(typeof(InvoiceTax))]
        public async Task<IHttpActionResult> PostInvoiceTax(InvoiceTax invoiceTax)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.InvoiceTaxes.Add(invoiceTax);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = invoiceTax.Id }, invoiceTax);
        }

        // DELETE: api/InvoiceTaxes/5
        [ResponseType(typeof(InvoiceTax))]
        public async Task<IHttpActionResult> DeleteInvoiceTax(int id)
        {
            InvoiceTax invoiceTax = await db.InvoiceTaxes.FindAsync(id);
            if (invoiceTax == null)
            {
                return NotFound();
            }

            db.InvoiceTaxes.Remove(invoiceTax);
            await db.SaveChangesAsync();

            return Ok(invoiceTax);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InvoiceTaxExists(int id)
        {
            return db.InvoiceTaxes.Count(e => e.Id == id) > 0;
        }
    }
}