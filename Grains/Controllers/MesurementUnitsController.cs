﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Grains.Models;

namespace Grains.Controllers
{
    public class MesurementUnitsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/MesurementUnits
        public IQueryable<MesurementUnit> GetMesurementUnits()
        {
            return db.MesurementUnits;
        }

        // GET: api/MesurementUnits/5
        [ResponseType(typeof(MesurementUnit))]
        public async Task<IHttpActionResult> GetMesurementUnit(int id)
        {
            MesurementUnit mesurementUnit = await db.MesurementUnits.FindAsync(id);
            if (mesurementUnit == null)
            {
                return NotFound();
            }

            return Ok(mesurementUnit);
        }

        // PUT: api/MesurementUnits/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutMesurementUnit(int id, MesurementUnit mesurementUnit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != mesurementUnit.Id)
            {
                return BadRequest();
            }

            db.Entry(mesurementUnit).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MesurementUnitExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/MesurementUnits
        [ResponseType(typeof(MesurementUnit))]
        public async Task<IHttpActionResult> PostMesurementUnit(MesurementUnit mesurementUnit)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MesurementUnits.Add(mesurementUnit);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = mesurementUnit.Id }, mesurementUnit);
        }

        // DELETE: api/MesurementUnits/5
        [ResponseType(typeof(MesurementUnit))]
        public async Task<IHttpActionResult> DeleteMesurementUnit(int id)
        {
            MesurementUnit mesurementUnit = await db.MesurementUnits.FindAsync(id);
            if (mesurementUnit == null)
            {
                return NotFound();
            }

            db.MesurementUnits.Remove(mesurementUnit);
            await db.SaveChangesAsync();

            return Ok(mesurementUnit);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MesurementUnitExists(int id)
        {
            return db.MesurementUnits.Count(e => e.Id == id) > 0;
        }
    }
}