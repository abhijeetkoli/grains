﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Grains.Models;

namespace Grains.Controllers
{
    public class InvoiceProductsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/InvoiceProducts
        public IQueryable<InvoiceProduct> GetInvoiceProducts()
        {
            return db.InvoiceProducts;
        }

        // GET: api/InvoiceProducts/5
        [ResponseType(typeof(InvoiceProduct))]
        public async Task<IHttpActionResult> GetInvoiceProduct(int id)
        {
            InvoiceProduct invoiceProduct = await db.InvoiceProducts.FindAsync(id);
            if (invoiceProduct == null)
            {
                return NotFound();
            }

            return Ok(invoiceProduct);
        }

        // PUT: api/InvoiceProducts/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutInvoiceProduct(int id, InvoiceProduct invoiceProduct)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != invoiceProduct.Id)
            {
                return BadRequest();
            }

            db.Entry(invoiceProduct).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!InvoiceProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/InvoiceProducts
        [ResponseType(typeof(InvoiceProduct))]
        public async Task<IHttpActionResult> PostInvoiceProduct(InvoiceProduct invoiceProduct)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.InvoiceProducts.Add(invoiceProduct);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = invoiceProduct.Id }, invoiceProduct);
        }

        // DELETE: api/InvoiceProducts/5
        [ResponseType(typeof(InvoiceProduct))]
        public async Task<IHttpActionResult> DeleteInvoiceProduct(int id)
        {
            InvoiceProduct invoiceProduct = await db.InvoiceProducts.FindAsync(id);
            if (invoiceProduct == null)
            {
                return NotFound();
            }

            db.InvoiceProducts.Remove(invoiceProduct);
            await db.SaveChangesAsync();

            return Ok(invoiceProduct);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool InvoiceProductExists(int id)
        {
            return db.InvoiceProducts.Count(e => e.Id == id) > 0;
        }
    }
}