﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Grains.Models
{
    public class MesurementUnit
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}