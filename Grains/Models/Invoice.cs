﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Grains.Models
{
    public class Invoice
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public double Price { get; set; }
        public double Tax { get; set; }
        public double Discount { get; set; }
        public double Cost { get; set; }
        
        public Order Order { get; set; }
        public Payment Payment { get; set; }
        

        public ICollection<InvoiceProduct> InvoiceProducts { get; set; }
    }
}