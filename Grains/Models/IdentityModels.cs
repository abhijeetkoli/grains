﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Data.Entity;

namespace Grains.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        
        public ApplicationDbContext()
            //: base(Helpers.GetRDSConnectionString())
            : base("GrainsRDS", throwIfV1Schema: false)
        {
        }
        
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }


        public DbSet<Category> Categories { get; set; }

        public DbSet<Manufacturer> Manufacturers { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<OrderProduct> OrderProducts { get; set; }

        public DbSet<OrderStatus> OrderStatus { get; set; }

        public DbSet<Payment> Payments { get; set; }

        public DbSet<PaymentMode> PaymentModes { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Rate> Rates { get; set; }

        public DbSet<Tax> Taxes { get; set; }

        public DbSet<TaxType> TaxTypes { get; set; }

        public System.Data.Entity.DbSet<Grains.Models.Frequency> Frequencies { get; set; }

        public System.Data.Entity.DbSet<Grains.Models.Invoice> Invoices { get; set; }

        public System.Data.Entity.DbSet<Grains.Models.InvoiceProduct> InvoiceProducts { get; set; }

        public System.Data.Entity.DbSet<Grains.Models.InvoiceTax> InvoiceTaxes { get; set; }

        public System.Data.Entity.DbSet<Grains.Models.MesurementUnit> MesurementUnits { get; set; }
    }
}