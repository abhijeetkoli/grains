﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Grains.Models
{
    public class Payment
    {
        public int Id { get; set; }
        public string TransactionId { get; set; }
        public double Amount { get; set; }

        public PaymentMode PaymentMode { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}