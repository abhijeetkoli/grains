﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Grains.Models
{
    public class InvoiceTax
    {
        public int Id { get; set; }
        public string Rate { get; set; }
        public double Amount { get; set; }
        
        public Invoice Invoice { get; set; }
    }
}