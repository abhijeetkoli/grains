﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Grains.Models
{
    public class Manufacturer
    {
        public int Id { get; set; }
        [MaxLength(500)]
        public string Name { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}