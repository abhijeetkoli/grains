﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Grains.Models
{
    public class OrderProduct
    {
        public int Id { get; set; }
        public double Quantity { get; set; }

        public MesurementUnit Unit { get; set; }
        public Order Order { get; set; }
        public Product Product { get; set; }
    }
}