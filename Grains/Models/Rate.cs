﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Grains.Models
{
    public class Rate
    {
        public int Id { get; set; }
        public double Price { get; set; }
        public DateTime PublishOn { get; set; }
        public bool Published { get; set; }
        public bool Deleted { get; set; }
       
        public Product Product { get; set; }

    }
}