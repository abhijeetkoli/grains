﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Grains.Models
{
    public class TaxType
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Tax> Taxes { get; set; }
    }
}