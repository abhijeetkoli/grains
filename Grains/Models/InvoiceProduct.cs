﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Grains.Models
{
    public class InvoiceProduct
    {
        public int Id { get; set; }
        public double Quantity { get; set; }
        public double Rate { get; set; }
        public double Cost { get; set; }

        public MesurementUnit Unit { get; set; }
        public Invoice Invoice { get; set; }
        public Product Product { get; set; }
    }
}