﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Grains.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }

        public ApplicationUser ApplicationUser { get; set; }
        public Manufacturer Manufacturer { get; set; }
        public Category Category { get; set; }
    }
}