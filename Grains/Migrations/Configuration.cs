namespace Grains.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Grains.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Grains.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Categories.AddOrUpdate(
                c => c.Name,
                new Models.Category { Name = "Rice"},
                new Models.Category { Name = "Oil" },
                new Models.Category { Name = "Daal" },
                new Models.Category { Name = "Floure" },
                new Models.Category { Name = "Grains" }
           );

            context.MesurementUnits.AddOrUpdate(
                mu => mu.Name,
                new Models.MesurementUnit { Name = "KG" },
                new Models.MesurementUnit { Name = "GM" },
                new Models.MesurementUnit { Name = "L" },
                new Models.MesurementUnit { Name = "ML" }
                );

            context.TaxTypes.AddOrUpdate(
                tt => tt.Name,
                new Models.TaxType { Name = "Percentage" },
                new Models.TaxType { Name = "Amount" }
                );

            context.Frequencies.AddOrUpdate(
                    f => f.Name,
                    new Models.Frequency { Name = "Daily" },
                    new Models.Frequency { Name = "Weekly" },
                    new Models.Frequency { Name = "Monthly" },
                    new Models.Frequency { Name = "Yearly" }
                );

            context.OrderStatus.AddOrUpdate(
                os => os.Name,
                new Models.OrderStatus { Name = "Sent" },
                new Models.OrderStatus { Name = "Processing" },
                new Models.OrderStatus { Name = "Ready For Delivery" },
                new Models.OrderStatus { Name = "In Transit" },
                new Models.OrderStatus { Name = "Delivered" }
                );

            context.PaymentModes.AddOrUpdate(
                pm => pm.Name,
                new Models.PaymentMode { Name = "Cash" },
                new Models.PaymentMode { Name = "Card" },
                new Models.PaymentMode { Name = "PAYTM" }
                );
        }
    }
}
